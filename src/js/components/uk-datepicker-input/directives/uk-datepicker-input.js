angular.module('uk-toolkit.components')
    .directive('ukDatepickerInput', function($filter, $window) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            scope: {
                ngModel: '=', // Ng-Model
                placeholder: '@',
                dateFormat: '@',
                excludeWeekends: '=',
                excludeDays: '=',
                startDate:'=',
                minDate :'=',
                ngChange: '&', //Call when the date of the calendar changed!
                ngBlur: '&' //Call when the user out of the focus (BLUR)

            },
            transclude: false,
            templateUrl: 'uk-datepicker-input/uk-datepicker-input.tpl.html',
            link: function(scope, elm, attrs, ctrls) {
                var ctrl = ctrls[0];
                ctrl.$validators.isDate = function(modelValue, viewValue) {
                    // because we always set null when model is invalid :P!
                    // easy cake!.
                    return modelValue !== null;
                };
            },
            controller: function($scope, $element, $q) {
                var container = $element.find("material-input");
                var windowDOM = window.angular.element($window);
                var focusActiveClass = "uk-focus-active";

                // ------------------------------
                // Model && default values
                // ------------------------------
                var vm = $scope.vm = {
                    // Validation Flag
                    isValidityValid: true,
                    checkFocus: false
                };



                vm.startDate = ($scope.startDate) ? $scope.startDate : null;
                vm.minDate = ($scope.minDate) ? $scope.minDate : null;
                vm.excludeDays=($scope.excludeDays) ? $scope.excludeDays : [];
                vm.excludeWeekends=($scope.excludeWeekends) ? $scope.excludeWeekends : false;
                
                // ------------------------------
                // Private Method's
                // ------------------------------
                var writeValue = function(value) {

                    var format = ($scope.dateFormat || 'dd-MM-yyyy');

                    if (value !== null && value !== undefined) {
                        vm.isValidityValid = true;
                        var tomorrowDate = new Date();
                        /*tomorrowDate.setDate(tomorrowDate.getDate() + 1);
                        date1 = Date.UTC(value.getFullYear(), value.getMonth(), value.getDate());
                        date2 = Date.UTC(tomorrowDate.getFullYear(), tomorrowDate.getMonth(), tomorrowDate.getDate());
                        if ((date1 - date2) < 0) {
                            value = tomorrowDate;
                        }*/

                        $scope.ngModel = vm.isValidityValid ? value : null;

                        
                        vm.viewValue = $filter('date')(value, format);
                    }
                };

                // ------------------------------
                // Exposed Method's
                // ------------------------------
                vm.isValid = function() {
                    return vm.isValidityValid;
                };

                vm.isFilled = function() {
                    var value = (vm.viewValue || '');
                    return value.toString().length > 0;
                };

                // ------------------------------
                // Event's
                // ------------------------------
                var closeCalendar = function() {
                    container.removeClass("uk-open uk-datepicker-focus");
                };
                windowDOM.on('click', closeCalendar);

                vm.cancelPropagation = function(ev) {
                    ev.stopPropagation();
                    ev.preventDefault();
                };
                vm.onFocus = function() {
                    vm.checkFocus = true;
                    $element.addClass(focusActiveClass);
                    angular.element(document.querySelectorAll('.uk-open')).removeClass('uk-open uk-datepicker-focus');

                    container.addClass("uk-open uk-datepicker-focus ");

                    if (vm.isValidityValid) {
                        //vm.viewValue = $scope.ngModel;
                    }
                };
                vm.onBlur = function() {
                    vm.checkFocus = true;
                    $element.removeClass(focusActiveClass);
                    if (vm.isValidityValid) {

                    }

                    var handler = $scope.ngBlur();
                    if (handler && vm.checkFocus) {
                        handler();
                    }
                    vm.checkFocus = false;
                };
                vm.onChangeDate = function(date) {
                    writeValue(date);

                    closeCalendar();

                     var handler = $scope.ngChange();
                    if (handler) {
                        handler(date);
                    }
                };
                vm.onChange = writeValue;

                $scope.$watch("ngModel", function(value) {
                    writeValue(value);

                    if (!vm.isValidityValid && value) {
                        vm.viewValue = value;
                    } else {
                        //Only "format" when user is not edite the control
                        if (!$element.hasClass(focusActiveClass)) {
                            vm.onBlur();
                        }
                    }

                });


                // ------------------------------
                // Destroy
                // ------------------------------
                $scope.$on("$destroy", function() {
                    windowDOM.off('click', closeCalendar);
                });
            }
        };
    });