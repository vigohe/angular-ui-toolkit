angular.module('uk-toolkit.components')
    .directive('ukOptionItems', function($compile) {
        return {
            restrict: 'E',
            require: ['ngModel'],
            template: '',
            scope: {
                ngModel: '=', // Ng-Model
                itemCollection: '=',
                optionTemplate: '@',
                optionChange: '&'
            },
            compile: function() {
                return {             
                    pre: function(scope, element, attrs, controller, transcludeFn) {


                        //---------------------------------------------------
                        // Bind Template Html
                        var fragment = [
                            '<div ng-repeat="item in itemCollection">',
                            '   <a href="" ng-click="select(item)" class="uk-button uk-button-large uk-button-radio" ng-class="{\'uk-active\': isEqual(item)}">',
                            scope.optionTemplate,
                            '   </a>',
                            '</div>'
                        ].join("");

                        element.append($compile(fragment)(scope));
                        //---------------------------------------------------


                    }         
                };
            },
            controller: function($scope, $element, $compile) {

                //---------------------------------------------------
                // Bind Template Html
                $scope.select = function(item) {
                    $scope.ngModel = item;

                    var handler = $scope.optionChange();
                    if (handler) {
                        handler(item);
                    }
                };
                //---------------------------------------------------

                $scope.isEqual = function(itemToCompare) {
                    return angular.equals($scope.ngModel, itemToCompare);
                };

            }
        };
    });